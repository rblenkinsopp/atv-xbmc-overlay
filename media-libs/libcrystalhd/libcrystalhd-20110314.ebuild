# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=4

EGIT_REPO_URI="git@bitbucket.org:XenoPhoenix/crystalhd.git https://bitbucket.org/XenoPhoenix/crystalhd.git"
EGIT_SOURCEDIR="${WORKDIR}/${PN}"

if [[ ${PV} == "9999" ]] ; then
	EGIT_COMMIT="master"
else
	EGIT_COMMIT="fdd2f19ac739a3db1fd7469ea19ceaefe0822e5a"
fi


inherit eutils git-2

DESCRIPTION="Broadcom Crystal HD library"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND="=media-video/crystalhd-driver-${PV}"
RDEPEND="${DEPEND}"
S="${WORKDIR}/${PN}/linux_lib/libcrystalhd"

src_prepare() {
	cd "../../"
	epatch "${FILESDIR}/${P}-use-8-dma-buffers.patch"
	epatch "${FILESDIR}/${P}-fix-pause-resume.patch"
	cd "${S}"
}
