ATV XBMC Overlay
================

About
-----

A [Gentoo GNU/Linux][gentoo] portage overlay for [XBMC][xbmc].
While this is primarily designed for the [1st generation Apple TV][appletv], the ebuilds have no hardcoded options for this; instead all the packages use standard USE flags. Therefore this overlay should be usable on any standard Gentoo system.

General packages that are included in this overlay include:

 * **media-tv/xbmc**                - [XBMC Media Centre][xbmc]
 * **media-libs/libcrystalhd**      - Library for the [Broadcom CrystalHD][crystalhd] decoder cards.
 * **media-video/crystalhd-driver** - Kernel module for [Broadcom CrystalHD][crystalhd] decoder cards.

Apple TV specific packages include:

 * **app-misc/atvclient**           - Small client for controlling the Apple TV LED and setting remote codes to XBMC.
 * **sys-boot/atv-bootloader**      - Apple TV bootloader.
 * **sys-devel/darwin-cross**       - Darwin cross-compilation toolchain.

[gentoo]:    http://www.gentoo.org                                    "Gentoo Linux"
[xbmc]:      http://www.xbmc.org                                      "XBMC Media Centre"
[appletv]:   http://support.apple.com/kb/SP19                         "Apple TV (1st Generation)"
[crystalhd]: http://www.broadcom.com/products/features/crystal_hd.php "Broadcom CrystalHD"
