# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils

DESCRIPTION="Darwin cross-compiler tools for i386"
HOMEPAGE="http://code.google.com/p/atv-bootloader"
SRC_URI="http://atv-bootloader.googlecode.com/files/darwin-cross.tar.gz"

LICENSE="APSL-2 GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND="app-arch/tar"
RDEPEND="${DEPEND}"
S="${WORKDIR}/${PN}"

src_install() {
	dodir /opt
	tar xfz "${S}/${PN}.tar.gz" -C "${D}/opt/" || die "Install failed!"

	# Remove annoying .DS_Store files as this was packages on the mac.
	find ${D} -name ".DS_Store" -delete
}

