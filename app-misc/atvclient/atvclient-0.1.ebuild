# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

EGIT_REPO_URI="git://github.com/Evinyatar/atvclient.git git://github.com/Evinyatar/atvclient.git"

if [[ ${PV} == "9999" ]] ; then
	EGIT_COMMIT="master"
else
	EGIT_COMMIT="3748257cc87b855ac4764209217f863d1795b7c7"
fi

inherit eutils git-2

DESCRIPTION="AppleTV remote event client"
HOMEPAGE="https://github.com/Evinyatar/atvclient"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND=">=dev-libs/libusb-compat-0.1.3";
RDEPEND="${DEPEND}"

src_install() {
	emake DESTDIR="${D}" install
	newconfd ${FILESDIR}/atvclient.conf atvclient
	newinitd ${FILESDIR}/atvclient.init atvclient
	dodoc AUTHORS INSTALL NEWS README ChangeLog
}
