# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

ESVN_REPO_URI="http://atv-bootloader.googlecode.com/svn/trunk/@520"

inherit eutils subversion

DESCRIPTION="Apple TV bootloader"
HOMEPAGE="http://code.google.com/p/atv-bootloader"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}/${P}-silence-output.patch"
}

src_compile() {
	return
}

src_install() {
	dodir /usr/src/atv-bootloader
	cp -PR "${S}/." "${D}/usr/src/atv-bootloader/" || die "Install failed!"
	dodoc README || die
}

