# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-tv/xbmc/xbmc-9999.ebuild,v 1.93 2011/10/12 22:53:27 vapier Exp $

EAPI=4

inherit eutils python autotools

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	if use cec ; then
		EGIT_REPO_URI="git://github.com/xbmc/xbmc.git https://github.com/xbmc/xbmc.git"
	else
		EGIT_REPO_URI="git://github.com/Pulse-Eight/xbmc.git https://github.com/Pulse-Eight/xbmc.git"
	fi
else
	if use cec ; then
		SRC_URI="https://github.com/Pulse-Eight/xbmc/tarball/master"
	else
		SRC_URI="http://mirrors.xbmc.org/releases/source/${P}.tar.gz"
	fi
fi

DESCRIPTION="XBMC is a free and open source media-player and entertainment hub"
HOMEPAGE="http://xbmc.org/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="airplay alsa altivec avahi bluray cec crystalhd css debug gles goom joystick
libusb midi nfs optimization profile +projectm pulseaudio +rsxs rtmp +samba sse sse2
udev vaapi vdpau vorbis webserver +xrandr"

COMMON_DEPEND="virtual/opengl
	app-arch/bzip2
	app-arch/unzip
	app-arch/zip
	app-i18n/enca
	airplay? (
		app-pda/libplist
	)
	>=dev-lang/python-2.4
	dev-libs/boost
	dev-libs/fribidi
	dev-libs/libcdio[-minimal]
	dev-libs/libpcre[cxx]
	>=dev-libs/lzo-2.04
	dev-libs/yajl
	>=dev-python/pysqlite-2
	media-libs/alsa-lib
	media-libs/flac
	media-libs/fontconfig
	media-libs/freetype
	>=media-libs/glew-1.5.6
	media-libs/jasper
	media-libs/jbigkit
	virtual/jpeg
	>=media-libs/libass-0.9.7
	bluray? ( media-libs/libbluray )
	css? ( media-libs/libdvdcss )
	media-libs/libmad
	media-libs/libmodplug
	media-libs/libmpeg2
	media-libs/libogg
	media-libs/libpng
	projectm? ( media-libs/libprojectm )
	media-libs/libsamplerate
	media-libs/libsdl[audio,opengl,video,X]
	alsa? ( media-libs/libsdl[alsa] )
	media-libs/libvorbis
	media-libs/sdl-gfx
	>=media-libs/sdl-image-1.2.10[gif,jpeg,png]
	media-libs/sdl-mixer
	media-libs/sdl-sound
	media-libs/tiff
	pulseaudio? ( media-sound/pulseaudio )
	media-sound/wavpack
	>=virtual/ffmpeg-0.6
	rtmp? ( media-video/rtmpdump )
	avahi? ( net-dns/avahi )
	webserver? ( net-libs/libmicrohttpd )
	net-misc/curl
	samba? ( >=net-fs/samba-3.4.6[smbclient] )
	sys-apps/dbus
	sys-libs/zlib
	virtual/mysql
	x11-apps/xdpyinfo
	x11-apps/mesa-progs
	vaapi? ( x11-libs/libva )
	vdpau? (
		|| ( x11-libs/libvdpau >=x11-drivers/nvidia-drivers-180.51 )
		virtual/ffmpeg[vdpau]
	)
	crystalhd? ( media-libs/libcrystalhd )
	cec? ( dev-libs/libcec )
	udev? ( sys-fs/udev )
	nfs? ( net-fs/libnfs )
	dev-libs/libusb
	x11-libs/libXinerama
	xrandr? ( x11-libs/libXrandr )
	vorbis? ( media-libs/libvorbis )
	x11-libs/libXrender"
RDEPEND="${COMMON_DEPEND}
	udev? (	sys-fs/udisks sys-power/upower )"
DEPEND="${COMMON_DEPEND}
	dev-util/gperf
	x11-proto/xineramaproto
	dev-util/cmake
	x86? ( dev-lang/nasm )"

src_unpack() {
	if use cec ; then
		einfo "USE flag 'cec' is enabled, CEC enabled sources will be fetched"
		einfo "instead of vanilla ones, add -cec to xbmc to disable this."
	fi
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
		cd "${S}"
		rm -f configure
	else
		unpack ${A}
		cd "${S}"
	fi

	# Fix case sensitivity
	mv media/Fonts/{a,A}rial.ttf || die
	mv media/{S,s}plash.png || die
}

src_prepare() {
	# some dirs ship generated autotools, some dont
	local d
	for d in \
		. \
		lib/{libdvd/lib*/,cpluff,libapetag,libid3tag/libid3tag} \
		xbmc/screensavers/rsxs-* \
		xbmc/visualizations/Goom/goom2k4-0
	do
		[[ -e ${d}/configure ]] && continue
		pushd ${d} >/dev/null
		einfo "Generating autotools in ${d}"
		eautoreconf
		popd >/dev/null
	done

	# Clean generated files.
	find . -depth -type d -name "autom4te.cache" -exec rm -rf {} \;

	local squish #290564
	use altivec && squish="-DSQUISH_USE_ALTIVEC=1 -maltivec"
	use sse && squish="-DSQUISH_USE_SSE=1 -msse"
	use sse2 && squish="-DSQUISH_USE_SSE=2 -msse2"
	sed -i \
		-e '/^CXXFLAGS/{s:-D[^=]*=.::;s:-m[[:alnum:]]*::}' \
		-e "1iCXXFLAGS += ${squish}" \
		lib/libsquish/Makefile.in || die

	# Fix XBMC's final version string showing as "exported"
	# instead of the SVN revision number.
	export HAVE_GIT=no GIT_REV=${EGIT_VERSION:-exported}

	# Avoid lsb-release dependency
	sed -i \
		-e 's:lsb_release -d:cat /etc/gentoo-release:' \
		xbmc/utils/SystemInfo.cpp || die

	# avoid long delays when powerkit isn't running #348580
	sed -i \
		-e '/dbus_connection_send_with_reply_and_block/s:-1:3000:' \
		xbmc/linux/*.cpp || die

	epatch "${FILESDIR}/${P}-headers.patch"

	epatch_user #293109

	# Tweak autotool timestamps to avoid regeneration
	find . -type f -print0 | xargs -0 touch -r configure
}

src_configure() {
	# Disable documentation generation
	export ac_cv_path_LATEX=no
	# Avoid help2man
	export HELP2MAN=$(type -P help2man || echo true)

	econf \
		--docdir=/usr/share/doc/${PF} \
		--enable-external-libraries \
		--enable-external-ffmpeg \
		--enable-gl \
		--enable-sdl \
		--enable-x11 \
		--enable-texturepacker \
		--enable-non-free \
		--disable-hal \
		--disable-mid \
		--disable-optical-drive \
		--disable-vdadecoder \
		--disable-vtbdecoder \
		--disable-openmax \
		--disable-tegra \
		--disable-asap-codec \
		$(use_enable libusb) \
		$(use_enable gles) \
		$(use_enable debug) \
		$(use_enable udev) \
		$(use_enable optimization optimizations) \
		$(use_enable airplay) \
		--disable-airtunes \
		$(use_enable avahi) \
		$(use_enable bluray libbluray) \
		$(use_enable css dvdcss) \
		$(use_enable debug) \
		$(use_enable goom) \
		$(use_enable joystick) \
		$(use_enable midi mid) \
		$(use_enable profile profiling) \
		$(use_enable projectm) \
		$(use_enable pulseaudio pulse) \
		$(use_enable rsxs) \
		$(use_enable rtmp) \
		$(use_enable nfs) \
		$(use_enable samba) \
		$(use_enable vaapi) \
		$(use_enable vdpau) \
		$(use_enable webserver) \
		$(use_enable xrandr) \
		$(use_enable crystalhd) \
		$(use_enable cec libcec) \
		$(use_enable vorbis ffmpeg-libvorbis)
}

src_install() {
	emake install DESTDIR="${D}" || die
	dodoc keymapping.txt README.linux
	rm "${D}"/usr/share/doc/${PF}/{copying.txt,LICENSE.GPL} || die

	insinto /usr/share/applications
	doins tools/Linux/xbmc.desktop
	doicon tools/Linux/xbmc.png

	insinto "$(python_get_sitedir)" #309885
	doins tools/EventClients/lib/python/xbmcclient.py || die
	newbin "tools/EventClients/Clients/XBMC Send/xbmc-send.py" xbmc-send || die
}

pkg_postinst() {
	elog "Visit http://wiki.xbmc.org/?title=XBMC_Online_Manual"
}
